<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;

class CourseController extends Controller
{
    //
    function __construct()
    {
        $this->middleware('auth:api');
    }

       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Course::all();
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'limit' => 'integer|required|min:1',
        ]);

        $course = Course::create([
            'name' => $request->name,
            'limit' => $request->limit,
        ]);

        if ($course){
            return response()->json([
                'status' => 'success',
                'message' => 'Course is created successfully',
                'course' => $course,
            ]);
        }
        return response()->json([
            'status' => 'Failed',
            'message' => 'Course Counld not be created',
            'course' => null
        ]);
    }


}
