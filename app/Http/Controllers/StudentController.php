<?php

namespace App\Http\Controllers;
use App\Models\User;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\Course;
use App\Models\StudentCourse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Notifications\EmailNotification;
use Exception;
use Illuminate\Support\Facades\Hash;
use Illuminate\Notifications\Messages\MailMessage;

class StudentController extends Controller
{
    //
    function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('auth.role:admin', ['only' => ['store']]);
        $this->middleware('auth.role:student', ['only' => ['subscribeInCourse','chooseFavoriteTeacher']]);
    }

           /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Student::with('user')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'age' => $request->age,
            'role' => 'student',
        ]);

        $student = $user->student()->create($request->all());

        //TODO ADD TRY AND CATCH

        if ($student){
            return response()->json([
                'status' => 'success',
                'message' => 'Student created successfully',
                'student' => $student,
            ]);
        }
        return response()->json([
            'status' => 'Failed',
            'message' => 'Student Counld not be created',
            'student' => null
        ]);
    }

    public function subscribeInCourse(Request $request)
    {
        $request->validate([
            'courseId' => 'required',
        ]);

        $course = Course::find($request->courseId);

        if(!$course){
            return response()->json([
                'status' => 'Failed',
                'message' => 'Course is not Found',
            ]);
        }

        $studentCount = $course->students()->count();
        if($studentCount == $course -> limit)       
            return response()->json([
                'status' => 'Failed',
                'message' => 'Course is full',
            ]);

        $user = Auth::user()->student;

        $user->courses()->attach($request->courseId);

        if (($course -> limit - $studentCount) == 1){
            // Notify the admin via email
            $admin = User::firstWhere('role','admin');
            try{
            $admin->notify(new EmailNotification((new MailMessage)
             ->line('Kindly be informed that the course '. $course->name.' is full'))
            );
        } catch(Exception $ex){

             }
        }    
    
        return response()->json([
                'status' => 'Sucess',
                'message' => 'Created successfully',
            ]);
    }

    public function chooseFavoriteTeacher(Request $request)
    {
        $request->validate([
            'teacherId' => 'required',
        ]);

        $teacher = Teacher::find($request->teacherId);

        if(!$teacher){
            return response()->json([
                'status' => 'Failed',
                'message' => 'Teacher not Found',
            ]);
        }

        $student = Auth::user()->student;

        $student->update([
            'favorite_teacher_id' => $request->teacherId
        ]);

        return response()->json([
                'status' => 'Sucess',
                'message' => 'Created successfully',
            ]);
    }

    public function getAllByCourseId(Request $request){
        $request->validate([
            'courseId' => 'required',
        ]);

        $course = Course::find($request -> courseId);
        if(!$course)
             return response()->json([
                'status' => 'Failed',
                'message' => 'course not Found',
            ]);

        $students = $course -> students()->get();
        
        return response()->json([
            'status' => 'Sucess',
            'message' => '',
            'students' => $students
        ]);
    }

    public function getAllByFavoriteTeacherId(Request $request){
        $request->validate([
            'teacherId' => 'required',
        ]);

        $teacher = Teacher::find($request -> teacherId);

        if(!$teacher)
             return response()->json([
                'status' => 'Failed',
                'message' => 'teacher is not Found',
            ]);

        $students = Student::where('favorite_teacher_id',$request->teacherId)->get();

        return response()->json([
            'status' => 'Sucess',
            'message' => '',
            'students' => $students
        ]);
    }
}