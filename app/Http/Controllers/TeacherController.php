<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Teacher;

class TeacherController extends Controller
{
    //

    function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('auth.role:admin', ['only' => ['store']]);
    }

               /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Teacher::with('user')->get();
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'age' => 'integer|min:30'
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'role' => 'teacher',
        ]);

        $store = $user->teacher()->create($request->all());

        if ($store){
            return response()->json([
                'status' => 'success',
                'message' => 'Teacher created successfully',
                'teacher' => $store,
            ]);
        }
        return response()->json([
            'status' => 'success',
            'message' => 'Teacher Counld not be created',
            'teacher' => null
        ]);
    }
}
