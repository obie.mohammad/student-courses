<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Course extends Model
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'limit',
    ];

    public function students()
    {
        return $this->belongsToMany(Student::class, 'student_course');
    }
}
