<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\TeacherController;
use App\Http\Controllers\CourseController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::post('register', 'register');
    Route::post('logout', 'logout');
});

Route::controller(StudentController::class)->prefix("students")->group(function () {
    Route::get('', 'index');
    Route::get('/GetAllByCourseId', 'getAllByCourseId');
    Route::get('/ByFavoriteTeacher', 'getAllByFavoriteTeacherId');
    Route::post('/create', 'store');
    Route::post('/subscribeInCourse', 'subscribeInCourse');
    Route::post('/chooseFavoriteTeacher', 'chooseFavoriteTeacher');
});

Route::controller(TeacherController::class)->prefix("teachers")->group(function () {
    Route::get('', 'index');
    Route::post('/create', 'store');
});

Route::controller(CourseController::class)->prefix("courses")->group(function () {
    Route::get('', 'index');
    Route::post('/create', 'store');
});